package com.example.sensors;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.widget.Button;

public class TelaPrincipal extends Activity {

	private Button btTelaGPS;
	private Button btTelaTime;
	private Intent troca;
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_principal);

		btTelaGPS = (Button) findViewById(R.id.btTelaGPS);
		btTelaGPS.setOnClickListener(new Button.OnClickListener(){

			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				troca = new Intent(TelaPrincipal.this, TelaGPS.class);
				TelaPrincipal.this.startActivity(troca);
				TelaPrincipal.this.finish();
			}        	
		});

		btTelaTime = (Button) findViewById(R.id.btTelaTime);
		btTelaTime.setOnClickListener(new Button.OnClickListener(){

			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				troca = new Intent(TelaPrincipal.this, TelaTime.class);
				TelaPrincipal.this.startActivity(troca);
				TelaPrincipal.this.finish();
			}        	
		}); 
	}
	
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}
}
