package com.example.sensors;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;

import android.content.Context;
import android.widget.Toast;

public class SensorTime extends Sensors{

	/*GregorianCalendar calendar = new GregorianCalendar();
		int min = calendar.get(Calendar.MINUTE);
		int hora = calendar.get(Calendar.HOUR_OF_DAY);
		int ano = calendar.get(Calendar.YEAR);
		int mes = calendar.get(Calendar.MONTH);
		int dia = calendar.get(Calendar.DAY_OF_MONTH);
		String tempo = hora+":"+min;
		String data = dia+"/"+mes+"/"+ano;
		textData.setText(data);
		textHora.setText(tempo);
		
	*/
	
	private GregorianCalendar calendar;
	private int hora;
	private int min;	
	private int seg;
	private int ano;
	private int mes;
	private int dia;
	private String date;	
	private Context context;
	
	SensorTime(Locale local, Context context){
		calendar = new GregorianCalendar(local);
		this.context = context;
	}
	
	public SensorTime(Context context) {
		// TODO Auto-generated constructor stub
		this.context = context;
	}
	
	public void init() {
		// TODO Auto-generated method stub
		status = true ;
		Toast.makeText(context, "Sensor Ativado!", Toast.LENGTH_SHORT).show();

	}

	
	@Override
	public void shutdown() {
		// TODO Auto-generated method stub
		status = false;
		Toast.makeText(context, "Sensor Desativado!", Toast.LENGTH_SHORT).show();
	}

	
	public String getDate(){
		calendar = new GregorianCalendar();
		return date = ""+ calendar.get(Calendar.DATE);
	}
	
		
	public GregorianCalendar getCalendar() {
		calendar = new GregorianCalendar();
		return calendar;
	}
	
	public int getHours() {
		calendar = new GregorianCalendar();
		hora = calendar.get(Calendar.HOUR_OF_DAY);
		return hora;
	}

	public int getMin() {
		calendar = new GregorianCalendar();
		min = calendar.get(Calendar.MINUTE);
		return min;
	}

	public int getSec() {
		calendar = new GregorianCalendar();
		seg = calendar.get(Calendar.SECOND);
		return seg;
	}

	public int getYear() {
		calendar = new GregorianCalendar();
		ano = calendar.get(Calendar.YEAR);
		return ano;
	}

	public int getMonth() {
		calendar = new GregorianCalendar();
		mes = calendar.get(Calendar.MONTH) + 1;
		return mes;
	}

	public int getDay() {
		calendar = new GregorianCalendar();
		dia = calendar.get(Calendar.DAY_OF_MONTH);
		return dia;		
	}

	public void messaegeDate(){
		if(status != false){
			Toast.makeText(context, "" +getDay() +"/" +getMonth() +"/" +getYear(), Toast.LENGTH_LONG).show();
		}else{
			Toast.makeText(context, "Sensor desativado!Ativi-o.", Toast.LENGTH_SHORT).show();
		}
	}
	
	public void messaegeHours(){
		if(status != false){
			Toast.makeText(context, "" +getHours() +":" +getMin() +":" +getSec() , Toast.LENGTH_LONG).show();
		}else{
			Toast.makeText(context, "Sensor desativado!Ativi-o.", Toast.LENGTH_SHORT).show();
		}
	}
}
