package com.example.sensors;

import android.app.Activity;

import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;


public class TelaGPS extends Activity {

	private Button btAtivar;
	private Button btDesativar;
	private Button btLocalizacao;
	private SensorGPS sensor = null;
	@Override    
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_gps);

		btAtivar = (Button) findViewById(R.id.btGPSAtivar);
		btAtivar.setOnClickListener(new Button.OnClickListener(){

			public void onClick(View arg0) {
				// TODO Auto-generated method stub

				if(sensor == null){
					sensor = new SensorGPS((LocationManager) getSystemService(Context.LOCATION_SERVICE), getBaseContext());
					sensor.init();		
				}else{
					sensor.init();
				}

			}        	
		});        

		btDesativar = (Button) findViewById(R.id.btGPSDesativar);
		btDesativar.setOnClickListener(new Button.OnClickListener(){

			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if(sensor == null){
					sensor = new SensorGPS((LocationManager) getSystemService(Context.LOCATION_SERVICE), getBaseContext());
					sensor.shutdown();
				}else{
					sensor.shutdown();
				}						    
			}        	
		});      

		btLocalizacao = (Button) findViewById(R.id.btGPSVisualizar);
		btLocalizacao.setOnClickListener(new Button.OnClickListener(){

			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if(sensor == null){
					sensor = new SensorGPS((LocationManager) getSystemService(Context.LOCATION_SERVICE), getBaseContext());
					sensor.message();
				}else{
					sensor.message();
				}						    
			}        	
		});      


	}

	public boolean onKeyDown(int teclaPrecionada, KeyEvent event){

		if(teclaPrecionada == KeyEvent.KEYCODE_BACK){
			Intent troca = new 	Intent(TelaGPS.this, TelaPrincipal.class);
			TelaGPS.this.startActivity(troca);
			TelaGPS.this.finish();
			
			return false;
		}
		return super.onKeyDown(teclaPrecionada, event);
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}
}
