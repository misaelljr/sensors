package com.example.sensors;

import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.widget.Toast;

public class SensorGPS extends Sensors {

	private LocationManager locationManager = null;
	private Location localization;
	private String bestProvider;
	private Context context;
	
	SensorGPS(LocationManager service_location, Context context){
	
		locationManager = service_location;
		LocationListener locationListener = new NovaLocalizacao();
		locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 3000, 5, locationListener);
		this.context = context;
	}
	
	// tempo minimo em milisegundo e distancia minima em metros
	SensorGPS(LocationManager service_location, long timeMin, float distanceMin, Context context){
		
		locationManager = service_location;
		LocationListener locationListener = new NovaLocalizacao();
		locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, timeMin, distanceMin, locationListener);
		this.context = context;
			
	}
	
	// ativa as op��es de localiza��o 
	public void init() {
		// TODO Auto-generated method stub
		
		bestProvider = locationManager.getBestProvider(new Criteria(),true);
		// retorna o provedor que fornece melhor localiza��o 
		localization = locationManager.getLastKnownLocation(bestProvider);
		Toast.makeText(context, "Sensor Ativado!", Toast.LENGTH_SHORT).show();
		status = true;
	}


	@Override
	public void shutdown() {
		// TODO Auto-generated method stub
		status = false;
		Toast.makeText(context, "Sensor Desativado!", Toast.LENGTH_SHORT).show();
	}
		
	class NovaLocalizacao implements LocationListener{

		public void onLocationChanged(Location location) {
			//Log.i("onLocationChanged Rhoney", "Latitude : "+location.getLatitude()+"Longitude : "+location.getLongitude());
		}

		public void onProviderDisabled(String arg0) {
			// TODO Auto-generated method stub
		}

		public void onProviderEnabled(String arg0) {
			// TODO Auto-generated method stub
		}

		
		public void onStatusChanged(String arg0, int arg1, Bundle arg2) {
			// TODO Auto-generated method stub
			
		}
	}

	
	public Location getLocation() {
		// TODO Auto-generated method stub
		if(status == true){
			return localization;
		}else{
			Toast.makeText(context, "Sensor desativado!Ative-o.", Toast.LENGTH_LONG).show();
			return null;
		}
		
	}
	
	public Double getLatitude(){
		if(status == true){
			return localization.getLatitude();
		}else{
			Toast.makeText(context, "Sensor desativado!Ative-o.", Toast.LENGTH_LONG).show();
			return null;
		}
		
	}

	public void message(){
		if(status == true){
			Toast.makeText(context, "Latitude:" +localization.getLatitude() +"\nLongitude:" +localization.getLongitude(), Toast.LENGTH_LONG).show();
		}else{
			Toast.makeText(context, "Sensor desativado!Ative-o.", Toast.LENGTH_LONG).show();
		}
	}
	
	public Double getLongitude(){
		if(status == true){
			return localization.getLongitude();
		}else{
			Toast.makeText(context, "Sensor desativado!Ative-o.", Toast.LENGTH_LONG).show();
			return null;
		}
		
	}
}
