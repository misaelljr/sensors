package com.example.sensors;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.Toast;

public class TelaTime extends Activity {

	private Button btAtivar;
	private Button btDesativar;
	private Button btTime;
	private RadioGroup tipos;
	private SensorTime sensor;


	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_time);

		sensor = new SensorTime(getBaseContext());
		
		btAtivar = (Button) findViewById(R.id.btTimeAtivar);
		btAtivar.setOnClickListener(new Button.OnClickListener(){

			
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				sensor.init();	

			}        	
		}); 

		btDesativar = (Button) findViewById(R.id.btTimeDesativar);
		btDesativar.setOnClickListener(new Button.OnClickListener(){

			public void onClick(View arg0) {
				// TODO Auto-generated method stub

				sensor.shutdown();

			}        	
		}); 

		btTime = (Button) findViewById(R.id.btTimeVisualizar);
		btTime.setOnClickListener(new Button.OnClickListener(){

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				tipos = (RadioGroup) findViewById(R.id.seletor);

				switch (tipos.getCheckedRadioButtonId()) {
				case R.id.item_1:sensor.messaegeDate();						
				break;
				case R.id.item_2:sensor.messaegeHours();
				break;
				default:Toast.makeText(getBaseContext(), "Seleciona uma op�ao de formata��o de time que deseja visualizar.", Toast.LENGTH_SHORT).show();
					break;
				}
			}});
	}

	public boolean onKeyDown(int teclaPrecionada, KeyEvent event){

		if(teclaPrecionada == KeyEvent.KEYCODE_BACK){
			Intent troca = new 	Intent(TelaTime.this, TelaPrincipal.class);
			TelaTime.this.startActivity(troca);
			TelaTime.this.finish();
			
			return false;
		}
		return super.onKeyDown(teclaPrecionada, event);
	}

	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}
}
